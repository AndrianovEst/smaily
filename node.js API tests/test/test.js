const request = require("request");
const expect = require("chai").expect;
const baseUrl = "http://188.166.39.106/todos";
const auth_key = "Smaily VGhyZWUgZXJyb3JzIHdhbGsgaW50byBhIGJhci4gVGhlIGJhcm1hbiBzYXlzLCAiTm9ybWFsbHkgSSdkIHRocm93IHlvdSBhbGwgb3V0LCBidXQgdG9uaWdodCBJJ2xsIG1ha2UgYW4gZXhjZXB0aW9uIg=="

//some uuid to test. Used both by PATCH and DELETE tests.
let uuid ="http://188.166.39.106/todos/e45a59f5-0e0f-4916-87c9-7e65d51c4e52";

describe('API unit tests', ()=> {
    it('POST add new task', ()=> {
        request.post({
            url: baseUrl,
            headers: {
                "Authorization" : auth_key,
                'Content-Type': 'application/json'
            },
            body:'{"title":"Test task"}'
        },
            function(error, response, body) {
            		expect(Object.keys(body).length).to.be.greaterThan(0);
                    expect(response.statusCode).to.equal(202);
            });
    });

    it('GET retrieve all tasks', ()=> {
        request.get({
            url: baseUrl,
            headers: {
                "Authorization" : auth_key
            }
        },
            function(error, response, body) {
            		expect(Object.keys(body).length).to.be.greaterThan(0);
                    expect(response.statusCode).to.equal(200);
                    console.log(body);
            });
    });

    it('PATCH change task completion status', ()=> {
        request.patch({
            url: uuid,
            headers: {
                "Authorization" : auth_key,
                'Content-Type': 'application/json'
            },
            body:'{"command":"complete"}'
        },
            function(error, response, body) {
                if(response.statusCode == 202){
                    expect(response.statusCode).to.equal(202);
                }else{
                    expect(response.statusCode).to.equal(204);
                }
            });
    });

    it('DELETE task from the db', ()=> {
        request.delete({
            url: uuid,
            headers: {
                "Authorization" : auth_key
            }
        },
            function(error, response, body) {
                    expect(response.statusCode).to.equal(202);
            });
    });
});
import axios from "axios";

/////////////////CONSTANTS/////////////////////
const GET_ALL_TASKS = "GET_ALL_TASKS";
const POST_TASK = "POST_TASK";
const CHANGE_STATUS = "CHANGE_STATUS";
const DELETE_TASK = "DELETE_TASK";

const auth_key = "Smaily VGhyZWUgZXJyb3JzIHdhbGsgaW50byBhIGJhci4gVGhlIGJhcm1hbiBzYXlzLCAiTm9ybWFsbHkgSSdkIHRocm93IHlvdSBhbGwgb3V0LCBidXQgdG9uaWdodCBJJ2xsIG1ha2UgYW4gZXhjZXB0aW9uIg=="

/////////////////ACTIONS//////////////
const getTasks = (tasks) => ({ type: GET_ALL_TASKS, tasks });
const addTask = (task) => ({ type: POST_TASK, task });
const changeStatus = (task) => ({ type: CHANGE_STATUS, task });
const taskDelete = (task) => ({ type: DELETE_TASK, task });

/////////////////REDUCER/////////////////////
export const initial = {
  tasks: []
};

const reducer = (state = initial, action) => {
  switch (action.type) {
    case GET_ALL_TASKS:
      return Object.assign({}, state, { tasks: action.tasks.tasks });
    case POST_TASK:
      let updatedTasks = [action.task].concat(state.tasks);
      return Object.assign({}, state, { tasks: updatedTasks });
    case CHANGE_STATUS:
      let newArr = state.tasks.map((task) => {
        if (task.uuid === action.task.uuid) task.is_completed = task.is_completed ? false : true;
        return task;
      });
      return Object.assign({}, state, { tasks: newArr });
    case DELETE_TASK:
      let arr = state.tasks.filter((task) => {
        return !(task.uuid === action.task.uuid);
      });
      return Object.assign({}, state, { tasks: arr });
    default:
      return state;
  }
};
export default reducer;


/////////////// ACTION DISPATCHER FUNCTIONS///////////////////
export const getAllTasks = () => dispatch => {
  axios.get("http://188.166.39.106/todos", {
    headers: {
      Authorization: auth_key
    }
  })
    .then((response) => {
      return response.data;
    })
    .then((tasks) => {
      dispatch(getTasks(tasks))
    })
    .catch((err) => {
      console.error.bind(err);
    })
};

export const postNewTask = (task) => dispatch => {
  axios.post('http://188.166.39.106/todos', '{"title":"' + task + '"}', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': auth_key,
    }
  })
    .then((response) => {
      return response.data;
    })
    .then((task) => {
      dispatch(addTask(task));
    })
    .catch((err) => {
      console.error.bind(err);
    })
};

export const putChangeStatus = (task, bool) => (dispatch) => {
  let boolToText = bool ? '"revert"' : '"complete"';
  axios.patch("http://188.166.39.106/todos/" + task.uuid, '{"command":' + boolToText + '}', {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': auth_key,
    },
    method: "PATCH"
  })
    .then((response) => {
      return response.data;
    })
    .then((task) => {
      dispatch(changeStatus(task));
    })
    .catch((err) => {
      console.error.bind(err);
    })
};

export const deleteTask = (task) => (dispatch) => {
  dispatch(taskDelete(task));
  axios.delete("http://188.166.39.106/todos/" + task.uuid, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': auth_key,
    },
    method: "PATCH"
  })
    .catch((err) => {
      console.error.bind(err);
    })
};
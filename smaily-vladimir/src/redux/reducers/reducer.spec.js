import * as t from './reducer'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

describe('Test Redux actions', () => {

    const initialState = {
        tasks: []
    }
    
    it('should dispatch action', () => {
        const addTask1 = { type: t.getAllTasks }
        const addTask2 = { type: t.deleteTask }
        const addTask3 = { type: t.postNewTask }
        const addTask4 = { type: t.putChangeStatus }
        const store = mockStore(initialState)
        store.dispatch(addTask1)
        store.dispatch(addTask2)
        store.dispatch(addTask3)
        store.dispatch(addTask4)
        const actions = store.getActions()
        expect(actions).toEqual([addTask1,addTask2,addTask3,addTask4])
        console.log(actions)
    })
})
import React from 'react';
import { connect } from 'react-redux';
import { putChangeStatus, deleteTask } from '../redux/reducers/reducer';
import { MDBBtn, MDBContainer, MDBTableBody, MDBTable } from 'mdbreact';
import './index.css';

const Task = (props) => {
  return (
    <MDBContainer>
      <MDBTable>
        <MDBTableBody>
          <tr>
            <td width="170">
              <MDBBtn className="center-me" color="blue" size="sm" onClick={() => {
                props.putChangeStatus(props.Obj, props.is_completed)
              }}>{props.is_completed ? "revert" : "complete"}
              </MDBBtn>
            </td>
            <td width="170">
              <MDBBtn className="center-me" color="blue" size="sm" onClick={() =>
                props.deleteTask(props.Obj)}>Delete
              </MDBBtn>
            </td>
            <td width="300" className="center-me">
              {props.Name}
            </td>
          </tr>
        </MDBTableBody>
      </MDBTable>
    </MDBContainer>
  );
};

const mapDispatch = { putChangeStatus, deleteTask };
export default connect(null, mapDispatch)(Task);
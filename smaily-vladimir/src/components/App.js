import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from './Header';
import Section from './Section';
import { getAllTasks } from '../redux/reducers/reducer';

class Home extends Component {
    componentDidMount() {
        this.props.getAllTasks();
    }
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <Header />
                <Section />
            </div>
        )
    }
}

const mapState = ({ tasks }) => ({ tasks });
const mapDispatch = { getAllTasks };
export default connect(mapState, mapDispatch)(Home);
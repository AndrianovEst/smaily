import React from 'react';
import { connect } from 'react-redux';
import { postNewTask } from '../redux/reducers/reducer';
import { MDBBtn, MDBContainer, MDBTableHead, MDBTable } from 'mdbreact';

const Header = (props) => {
  return (
    <MDBContainer>
      <div className="content container text-center">
        <h1>To-Do App!</h1>
        <form onSubmit={evt => {
          evt.preventDefault();
          props.postNewTask(evt.target.taskName.value);
          evt.target.taskName.value = "";
          }
        }>
          <div>
            <label>Add New To-Do</label>
            <input autoComplete="off" className="form-control input-lg" name="taskName" placeholder="Enter new task" />
          </div>
          <MDBBtn type="submit">Add</MDBBtn>
        </form>
      </div>
      <MDBTable>
        <MDBTableHead >
          <tr>
            <th>Status</th>
            <th>Delete</th>
            <th className="center-me">Description</th>
          </tr>
        </MDBTableHead>
      </MDBTable>
    </MDBContainer>
  );
};

const mapDispatch = { postNewTask };
export default connect(null, mapDispatch)(Header);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Task from './Task';

class Section extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <section id="one" className="wrapper style2 special flow">
        {
          this.props.tasks && this.props.tasks.map(function (task, i) {
            if (task)
              return (
                <div key={i}>
                  <Task key={i} Obj={task} is_completed={task.is_completed} Name={task.title} />
                </div>
                )
            })
          }
        </section>
      );
    }
  };

const mapState = ({ tasks }) => ({ tasks });
export default connect(mapState)(Section);